#https://juliagraphs.org/Graphs.jl/dev/algorithms/community/
import Pkg
Pkg.add("Graphs")
Pkg.add("Random")
Pkg.add("SimpleTraits")
Pkg.add("StatsBase")
using StatsBase
using SimpleTraits
using Graphs, Random

function beklikk(aktualis_klikk, g)
    ind = length(aktualis_klikk)
    for i in range(1, ind-1)
        for j in range(i + 1, ind)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                return false
            end
        end
    end
    return true
end
#hianyzo elek szama
function hianyzo_el_db(aktualis_klikk, g)
    n = length(aktualis_klikk)
    db = 0
    for i in range(1, n-1)
        for j in range(i + 1, n)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                db+=1
            end
        end
    end
    return db
end

# a populacio mind csak klikkekbol all
function minden_klikk(populacio, g)
    mapreduce(t -> beklikk(t[1], g), &, populacio)
end

function eleme(x,t)
    findall(a->a==x, t) != []
end

function suly(klikk, weigths)
    total = 0
    for cs in klikk
        total = total + weigths[cs][1]
    end
    total
end
# kiveszi a legjobb klikket
function legjobb_klikk(populacio, g, sulyok)
    sorted = sort(populacio, by = t->hianyzo_el_db(t[1], g))
    sort(sorted, by = t-> suly(t[1], sulyok), rev=true)[1]
end

# klikk, nem_klikk-ben visszateriti a csucspontokat(sorszamukat)
function szetvalogat(egyed, g)
    v = vertices(g)
    klikk = []
    nem_klikk = []
    for i in 1:nv(g)
        if egyed[i]==1
            push!(klikk, v[i])
        else
            push!(nem_klikk, v[i])
        end
    end

    (klikk, nem_klikk)
end
# mindegyit egyedre eloallit egy klikk es nem_klikk listat
function toArrays(populacio, g)
    map(x->szetvalogat(x, g), populacio)
end
function generate_prob(mu)
    rng = MersenneTwister(1234);
    k = randexp(rng, Float32, mu)
    s = sum(k)
    sort(map(x->x/s, k), rev =true)
end
function DE_mutacio_megtart_elemszam(populacio, mu, n_klikk, pc)
    p = generate_prob(mu)
     [
        begin
            r1 = sample(populacio, Weights(p) , 1)[1]
            r2 = rand(populacio)
            r3 = rand(populacio)
            uj = map((e1, e2, e3) ->
                    if e2 == e3 || rand()<pc
                        e1
                    else
                        e2
                    end
                , r1, r2, r3)

            n = length(uj)
            while count(x-> x==1, uj) > n_klikk
                i = rand(1:n)
                while uj[i]==0
                    i = rand(1:n)
                end
                uj[i] = 0
            end

            while count(x-> x==1, uj) < n_klikk
                i = rand(1:n)
                while uj[i]==1
                    i = rand(1:n)
                end
                uj[i] = 1
            end

            uj
        end
        for i in 1:mu
    ]
end
# jobb-e az klikk1 mint a klikk2?
function jobb(klikk1, klikk2, g, sulyok)
    h1 = hianyzo_el_db(klikk1, g)
    h2 = hianyzo_el_db(klikk2, g)
    if suly(klikk1, sulyok) > suly(klikk2, sulyok)
        return true
    elseif suly(klikk1, sulyok) == suly(klikk2, sulyok)
        if h1 == h2
            return length(klikk1) > length(klikk2)
        else
            return h1 < h2
        end
    else
        return false
    end

end
# a regi egyedekbol es leszarmazottakbol letrehozza az uj populaciot
function DE_szelekcio(populacio, u, g, sulyok)
    map((egyed1, egyed2) ->
        if jobb(szetvalogat(egyed1, g)[1], szetvalogat(egyed2, g)[1], g, sulyok)
            egyed1
        else
            egyed2
        end,
        populacio, u)
end
function DE(kezdeti_populcaio, sulyok, g, n_klikk, pc, lep)
    populacio = copy(kezdeti_populcaio)
    mu = length(populacio)
    while lep>0 && !minden_klikk(toArrays(populacio, g), g)
        sort(populacio, lt = (x,y) -> jobb(szetvalogat(x, g)[1], szetvalogat(y, g)[1], g, sulyok))
        v = DE_mutacio_megtart_elemszam(populacio, mu, n_klikk, pc)
        populacio = DE_szelekcio(populacio, v, g, sulyok)
        lep -= 1
    end

    # println("lepes = $lep")
    populacio
end
# ket csucspontot beletesz a klikkbe
function DE_init_populacio(nv, mu)
    [
    begin
        k = [0 for i in 1:nv]
        csp1 = rand(1:nv)
        csp2 = rand(1:nv)
        while csp1 == csp2
            csp2 = rand(1:nv)
        end
        k[csp1] = 1
        k[csp2] = 1
        k
    end
    for i in 1:mu ]
end
# egyetlen egyedben attesz egy nem-klikk csucspontot a klikkbe
function DE_tagit_populacio(t)
    n = length(t)
    egyed = copy(t)
    csp = rand(1:n)
    while egyed[csp]==1
        csp = rand(1:n)
    end
    egyed[csp] = 1
    egyed
end

function legjobb_klikk(populacio, g, sulyok)
    sorted = sort(populacio, by = t->hianyzo_el_db(t[1], g))
    sort(sorted, by = t-> suly(t[1], sulyok), rev=true)[1]
end

function suly1(klikk, weigths)
    total = 0
    for cs in klikk[1]
        total = total + weigths[cs][1]
    end
    total
end

function max_klikk_DE(g, sulyok, mu=20, pc=0.1, lep = 10000)
    n_klikk = 2
    n = nv(g)
    populacio = DE_init_populacio(n, mu)

    populacio = DE(populacio, sulyok, g, n_klikk, pc, lep)
    max_klikk = ([], [])

    while n_klikk<n && beklikk(szetvalogat(populacio[1], g)[1], g)
        akt_max_klikk = legjobb_klikk(toArrays(populacio, g), g, sulyok)
        if(suly1(akt_max_klikk, sulyok) > suly1(max_klikk, sulyok))
            max_klikk = akt_max_klikk
        end

        n_klikk+=1
        populacio = map(DE_tagit_populacio, populacio)
        populacio = DE(populacio, sulyok, g, n_klikk, pc, lep)
    end

    if n_klikk==n && beklikk(vertices(g), g)
        if(suly(vertices(g), sulyok) > suly(max_klikk, sulyok))
            max_klikk = vertices(g)
        end
    elseif beklikk(szetvalogat(populacio[1], g)[1], g)
        akt_max_klikk = legjobb_klikk(toArrays(populacio, g), g, sulyok)
        if(suly(akt_max_klikk, sulyok) > suly(max_klikk, sulyok))
            max_klikk = akt_max_klikk
        end
    end
    max_klikk[1]
end

function getName()
    "Maximum weigted clique - DIFFERENTIAL EVOLUTION"
end

struct MWC_DE_cliqueSettings
    mu :: Integer
    pc :: Real
    lep :: Integer
end

MWC_DE_cliqueSettings(args) = MWC_DE_cliqueSettings(args...)

function getStruct()
    MWC_DE_cliqueSettings
end


function getDefault()
    return Any[20, 0.8, 10000]
end

function run(setting :: MWC_DE_cliqueSettings, graph)
    sulyok = [i for i in range(1, nv(graph))]
    max_klikk_DE(graph, sulyok, setting.mu, setting.pc, setting.lep)
end
