import Pkg
Pkg.add("Graphs")
Pkg.add("Random")
Pkg.add("SimpleTraits")
Pkg.add("StatsBase")
using StatsBase
using SimpleTraits
using Graphs, Random

function beklikk(aktualis_klikk, g)
    ind = length(aktualis_klikk)
    for i in range(1, ind-1)
        for j in range(i + 1, ind)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                return false
            end
        end
    end
    return true
end
function hianyzo_el_db(aktualis_klikk, g)
    n = length(aktualis_klikk)
    db = 0
    for i in range(1, n-1)
        for j in range(i + 1, n)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                db+=1
            end
        end
    end
    return db
end
function valtoztat(akt_klikk, nem_klikk)
    uj_akt = copy(akt_klikk)
    uj_nem = copy(nem_klikk)
    csp1 = rand(1:length(uj_akt))
    csp2 = rand(1:length(uj_nem))
    uj_akt[csp1], uj_nem[csp2] = copy(uj_nem[csp2]), copy(uj_akt[csp1])
    (uj_akt, uj_nem)
end
function init_populacio(mu, g, akt_klikk, nem_klikk)
    [ begin
        (akt_klikk, nem_klikk) = valtoztat(akt_klikk, nem_klikk)
    end
    for i in [1:mu;] ]
end
function minden_klikk(populacio, g)
    mapreduce(t -> beklikk(t[1], g), &, populacio)
end
#minden klikkbol kreal db uj klikket
function kreal_klikkek(db, populacio)
    vcat([ [ valtoztat(klikk[1], klikk[2]) for i in [1:db;] ] for klikk in populacio ]...)
end
#visszaterit mu darab legjobb egyedet
function legjobb(mu, ossz, g)
    sort(ossz, by = t->hianyzo_el_db(t[1], g))[1:mu]
end
function mutacio(populacio, mu)
    uj_populacio = kreal_klikkek(5, populacio)
end
function eleme(x,t)
    findall(a->a==x, t) != []
end
function keresztez(klikk1, klikk2, osszcsp)
    klikk = []
    nem_klikk = []
    p = 0.2

    for csp in osszcsp
        hova = true

        if eleme(csp, klikk1) == eleme(csp, klikk2) || rand()>0.2
            hova = eleme(csp, klikk1)
        else
            hova = eleme(csp, klikk2)
        end

        if hova
            push!(klikk, csp)
        else
            push!(nem_klikk, csp)
        end
    end
    (klikk, nem_klikk)
end
# atmretezi az uj egyedet(t), ugy, hogz az altala reprezentalt klikk nk meretu legyen
function meretez(nk, t)
    (klikk, nem_klikk) = t
    while length(klikk) < nk
        csp = rand(1:length(nem_klikk))
        push!(klikk, nem_klikk[csp])
        deleteat!(nem_klikk, csp)
    end

    while length(klikk) > nk
        csp = rand(1:length(klikk))
        push!(nem_klikk, klikk[csp])
        deleteat!(klikk, csp)
    end

    (klikk, nem_klikk)
end
function keresztezes(populacio, mu, g)
    osszes = vertices(g)
    uj_populacio = [
    begin
        klikk1 = rand(populacio)
        klikk2 = rand(populacio)
        while klikk2==klikk1
            klikk2 = rand(populacio)
        end

        t = keresztez(klikk1[1], klikk2[1], vertices(g))    # t = (klikk, nem_klikk)

        meretez(length(klikk1[1]), t)
    end for i in 1:5*mu ]
end
function genetikus_programozas(kezdeti_populcaio, g, pm, lep)
    populacio = copy(kezdeti_populcaio)
    mu = length(populacio)
    while lep>0 && !minden_klikk(populacio, g)
        uj_populacio = []
        if rand() < pm
            uj_populacio = mutacio(populacio, mu)
        else
            uj_populacio = keresztezes(populacio, mu, g)
        end
        ossz = vcat(populacio,uj_populacio)
        populacio = copy(legjobb(mu, ossz, g))
        lep -= 1
    end

    populacio
end
function legjobb_klikk(populacio, g)
    sort(populacio, by = t->hianyzo_el_db(t[1], g))[1]
end
function tagit_populacio(t)
    (akt_klikk, nem_klikk) = t
    csp = rand(1:length(nem_klikk))
    push!(akt_klikk, nem_klikk[csp])
    deleteat!(nem_klikk, csp)
    (akt_klikk, nem_klikk)
end
function max_klikk_genetikus(g, mu=20, pm=0.95, lep = 1000)

    alma = sort(vertices(g), by = v->degree(g, v), rev = true)
    akt_klikk = alma[1:2]
    nem_klikk = alma[3:length(alma)]
    db_nem_klikk = copy(length(nem_klikk))

    populacio = init_populacio(mu, g, akt_klikk, nem_klikk)

    populacio = genetikus_programozas(populacio, g, pm, lep)

    while db_nem_klikk>1 && beklikk(legjobb_klikk(populacio, g)[1], g)
        max_klikk = copy(legjobb_klikk(populacio, g)[1])

        db_nem_klikk-=1
        populacio = map(tagit_populacio, populacio)
        populacio = genetikus_programozas(populacio, g, pm, lep)
    end

    if db_nem_klikk==1 && beklikk(vertices(g), g)
        max_klikk = vertices(g)
    elseif beklikk(legjobb_klikk(populacio, g)[1], g)
        max_klikk = copy(legjobb_klikk(populacio, g)[1])
    end
    max_klikk
end



struct MC_GA_Settings
    mu:: Integer
    pm:: Real
    lep:: Integer
end

MC_GA_Settings(args) = MC_GA_Settings(args...)

function run(setting :: MC_GA_Settings, graph)
	#TODO read graph in
    max_klikk_genetikus(graph, setting.mu, setting.pm, setting.lep)
end

function getStruct()
	return MC_GA_Settings
end


function getDefault()
    return Any[20, 0.95, 100]
end


function getName()
	"Max clique - Genetic algorithm"
end