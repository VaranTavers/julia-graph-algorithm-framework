struct b
    x:: String
    y:: Int
end

struct a
    valami::Integer
    xd:: b
    valami2::Integer
end

#THIS SHOULD BE ADDED TO EVERY FILE
a(args) = a(args...)
b(args) = b(args...)

function numberOfFieldsInType(myType)
    if !isstructtype(myType)
        return 1
    end
    mySum = 0
    for type in fieldtypes(myType)
        if type != String && type != Function
            mySum += numberOfFieldsInType(type)
        end
    end
    return mySum
end

function createStruct(myStruct, arrayOfValues)
    fieldTypes = fieldtypes(myStruct)
    arrayOfValues =convert(Vector{Any}, arrayOfValues)
    for i in range(1, length(fieldTypes))
        fieldType = fieldTypes[i]
        if isstructtype(fieldType) && fieldType != String
            nextArrayOfValues = splice!(arrayOfValues, i:i+numberOfFieldsInType(fieldType)-1)
            nextInstance = createStruct(fieldType, nextArrayOfValues)
            insert!(arrayOfValues, i, nextInstance)
        end
    end
    myStruct(arrayOfValues...)
end