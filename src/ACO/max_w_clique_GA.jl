#https://juliagraphs.org/Graphs.jl/dev/algorithms/community/
import Pkg
Pkg.add("Graphs")
Pkg.add("Random")
Pkg.add("SimpleTraits")
Pkg.add("StatsBase")
using StatsBase
using SimpleTraits
using Graphs, Random

function beklikk(aktualis_klikk, g)
    ind = length(aktualis_klikk)
    for i in range(1, ind-1)
        for j in range(i + 1, ind)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                return false
            end
        end
    end
    return true
end
#hianyzo elek szama
function hianyzo_el_db(aktualis_klikk, g)
    n = length(aktualis_klikk)
    db = 0
    for i in range(1, n-1)
        for j in range(i + 1, n)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                db+=1
            end
        end
    end
    return db
end
# ket random elem megcserelese a klikk es nem klikk elemek kozott
function valtoztat(akt_klikk, nem_klikk)
    uj_akt = copy(akt_klikk)
    uj_nem = copy(nem_klikk)
    csp1 = rand(1:(length(uj_akt)))
    csp2 = rand(1:(length(uj_nem)))
    uj_akt[csp1], uj_nem[csp2] = copy(uj_nem[csp2]), copy(uj_akt[csp1])
    (uj_akt, uj_nem)
end
function linearis(alfa, t0, k)
    t0/(1 + alfa*k)
end
function szimulalt_lehutes(lehutes, alfa, max_lep, t0, akt_klikk, nem_klikk, g)
    t = t0
    legjobb = copy(akt_klikk)
    legjobb_nem = copy(nem_klikk)
    k = 0

    while t>0 && k<max_lep && hianyzo_el_db(legjobb, g)>0
        (uj_klikk, uj_nem) = valtoztat(akt_klikk, nem_klikk)
        r = rand()
        db_uj = hianyzo_el_db(uj_klikk, g)
        db_regi = hianyzo_el_db(akt_klikk, g)
        if db_uj<db_regi || r<exp((db_regi-db_uj)/t)
            (akt_klikk, nem_klikk) = (uj_klikk, uj_nem)
        end
        t = lehutes(alfa, t0, k)

        if hianyzo_el_db(akt_klikk, g) < hianyzo_el_db(legjobb, g)
            legjobb = copy(akt_klikk)
            legjobb_nem = copy(nem_klikk)
        end
        k += 1
    end

    (legjobb, legjobb_nem)
end
function init_populacio(mu, g, akt_klikk, nem_klikk)
    [ begin
        (akt_klikk, nem_klikk) = valtoztat(akt_klikk, nem_klikk)
    end
    for i in [1:mu;] ]
end
# egy csucs hozzaadasa az akt_klikkhez, a nem_klikkbol
function bovit_pop(t, g)
    (akt_klikk, nem_klikk) = t
    csp = rand(1:length(nem_klikk))
    push!(akt_klikk, nem_klikk[csp])
    deleteat!(nem_klikk, csp)
    (akt_klikk, nem_klikk)
end
# a populacio mind csak klikkekbol all
function minden_klikk(populacio, g)
    mapreduce(t -> beklikk(t[1], g), &, populacio)
end
# kiveszi az elso mu elemet a pop-bol
function legjobb(mu, ossz, g, sulyok)
    sorted = sort(ossz, by = t->hianyzo_el_db(t[1], g))
    sort(sorted, by = t-> suly(t[1], sulyok), rev=true)[1:mu]
end
#minden egyedben valtoztat db poziciot random es ezek lesznek uj egyedekke
function kreal_egyedek(db, populacio)
    vcat([ [ valtoztat(klikk[1], klikk[2]) for i in [1:db;] ] for klikk in populacio ]...)
end
function eleme(x,t)
    findall(a->a==x, t) != []
end
# kreal minden egyedbol ujat, es ezekbol kivalasztja a mu legjobbat
function mutacio(populacio, mu)
    uj_populacio = kreal_egyedek(5, populacio)
#     ossz = vcat(populacio,uj_populacio)
#     copy(legjobb(mu, ossz, g))
end
# a klikket alkoto lista szamossagat nk-r ameretezi
function meretez(nk, t)
    (klikk, nem_klikk) = t
    while length(klikk) < nk
        csp = rand(1:length(nem_klikk))
        push!(klikk, nem_klikk[csp])
        deleteat!(nem_klikk, csp)
    end

    while length(klikk) > nk
        csp = rand(1:length(klikk))
        push!(nem_klikk, klikk[csp])
        deleteat!(klikk, csp)
    end
    (klikk, nem_klikk)
end
# az osszes csucspontot elosztja ha mindekettoeben benne van, klikkbe kerul kulonben nem_klikkbe => letrejon egy uj egyed
function keresztez(klikk1, klikk2, osszcsp)
    klikk = []
    nem_klikk = []
    p = 0.2

    for csp in osszcsp
        hova = true

        if eleme(csp, klikk1) == eleme(csp, klikk2) || rand()>0.2
            hova = eleme(csp, klikk1)
        else
            hova = eleme(csp, klikk2)
        end

        if hova
            push!(klikk, csp)
        else
            push!(nem_klikk, csp)
        end
    end
    (klikk, nem_klikk)
end
# kreal 5*mu random uj egyedet a populaciobol keresztezessel
function keresztezes(populacio, mu, g)
    # osszes = vertices(g)
    uj_populacio = [
    begin
        egyed1 = rand(populacio)
        egyed2 = rand(populacio)
        while egyed2==egyed1
            egyed2 = rand(populacio)
        end

        t = keresztez(egyed1[1], egyed2[1], vertices(g))    # t = (klikk, nem_klikk)

        meretez(length(egyed1[1]), t)
    end for i in 1:5*mu ]

end
# mutacio v. keresztezes altal boviti a populaciot, visszaallitja a populacio meretet, a legjobbakat megtartva
function genetikus_algo(kezdeti_populcaio, g, sulyok, pm, lep = 1000)
    populacio = copy(kezdeti_populcaio)
    mu = length(populacio)
    while lep>0 && !minden_klikk(populacio, g)
        uj_populacio = []
        if rand() < pm
            uj_populacio = mutacio(populacio, mu)
        else
            uj_populacio = keresztezes(populacio, mu, g)
        end
        ossz = vcat(populacio,uj_populacio)
        populacio = copy(legjobb(mu, ossz, g, sulyok))
        lep -= 1
    end
    populacio
end
#kiszamolja az akt_klikk sulyat
function suly(klikk, weigths)
    total = 0
    for cs in klikk
        total = total + weigths[cs][1]
    end
    total
end
# kiveszi a legjobb klikket
function legjobb_klikk(populacio, g, sulyok)
    sorted = sort(populacio, by = t->hianyzo_el_db(t[1], g))
    sort(sorted, by = t-> suly(t[1], sulyok), rev=true)[1]
end
function max_klikk_genetikus(g, sulyok, mu=20, pm=0.1, lep = 1000)

    raw_vertices = sort(vertices(g), by = v->degree(g, v), rev = true)
    akt_klikk = raw_vertices[1:2]
    nem_klikk = raw_vertices[3:length(raw_vertices)]
    db_nem_klikk = copy(length(nem_klikk))

    populacio = init_populacio(mu, g, akt_klikk, nem_klikk)

    populacio = genetikus_algo(populacio, g, sulyok, pm, lep)

    max_klikk = copy(legjobb_klikk(populacio, g, sulyok)[1])

    while db_nem_klikk>1 && beklikk(legjobb_klikk(populacio, g, sulyok)[1], g)
        w_akt = suly(max_klikk, sulyok)
        max_klikk_akt = copy(legjobb_klikk(populacio, g, sulyok)[1])
        w_uj = suly(max_klikk_akt, sulyok)
        if w_uj > w_akt
            max_klikk = copy(max_klikk_akt)
        end
        db_nem_klikk-=1
        populacio = map(t -> bovit_pop(t, g), populacio) # mindegyik egyedbe beveszunk egy uj csucsot
        populacio = genetikus_algo(populacio, g, sulyok, pm, lep)
    end

    best_klikk = legjobb_klikk(populacio, g, sulyok)
    if db_nem_klikk==1 && beklikk(vertices(g), g) && suly(vertices(g), sulyok) > suly(max_klikk, sulyok)
        max_klikk = vertices(g)
    elseif beklikk(best_klikk[1], g) && suly(best_klikk[1], sulyok) > suly(max_klikk, sulyok)
        max_klikk = copy(best_klikk[1])
    end
    max_klikk
end

function getName()
    "Maximum weigted clique - GENETIC ALGORITHM"
end

struct MWC_GA_cliqueSettings
    mu :: Integer
    pm :: Real
    lep :: Integer
end
MWC_GA_cliqueSettings(args) = MWC_GA_cliqueSettings(args...)

function getStruct()
    MWC_GA_cliqueSettings
end

function run(setting :: MWC_GA_cliqueSettings, graph)
    sulyok = [i for i in range(1, nv(graph))]
    max_klikk_genetikus(graph, sulyok, setting.mu, setting.pm, setting.lep)
end

function getDefault()
    return Any[20, 0.8, 30]
end
