import Pkg
Pkg.add("Graphs")
Pkg.add("Random")
Pkg.add("SimpleTraits")
Pkg.add("StatsBase")
using StatsBase
using SimpleTraits
using Graphs, Random

function max_klikk_egzakt(g)
    all = maximal_cliques(g)
    all[findmax(map(x->(length(x),x), all))[2]]
end

function getName()
    "Maximum clique"
end

struct cliqueSettings
end

cliqueSettings(args) = cliqueSettings(args...)

function getStruct()
    cliqueSettings
end

function getDefault()
    return []
end

function run(setting :: cliqueSettings, graph)
    max_klikk_egzakt(graph)
end