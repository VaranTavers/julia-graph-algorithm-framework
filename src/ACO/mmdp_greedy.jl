begin
	using Graphs
	using SimpleWeightedGraphs
	using Folds
	using Base.Iterators
	using CSV
	using DataFrames
	using GraphPlot
	using Statistics
	using Random
end

function calculate_mindist(g, vertices, min_distances)

	dist_sums = map(i -> map(j -> min_distances[i, j], vertices), vertices)

	minimum(map(sum,dist_sums))
end

function maxmindp_greedy_dist(g, k)
	min_dists = g.weights

	furthest = argmax(mean(min_dists, dims=2)[:])
	points = zeros(Int64, k)
	points[1] = furthest

	dists = min_dists[points[1], :]
	for i in 2:k
		furthest = argmax(dists)
		points[i] = furthest
		dists = map(minimum, zip(dists, min_dists[points[i], :]))
	end

	points
end

function vector_with(v, x)
	vv = copy(v)
	push!(vv, x)

	vv
end

function maxmindp_greedy_mindp(g, k)
	min_dists = g.weights

	furthest = argmax(mean(min_dists, dims=2)[:])
	points = zeros(Int64, k)
	points[1] = furthest

	dists = min_dists[points[1], :]
	for i in 2:k
		mindps = map(x -> calculate_mindist(g, vector_with(points[1:(i-1)], x), min_dists), 1:nv(g))
		furthest = argmax(mindps)
		points[i] = furthest
		dists = map(minimum, zip(dists, min_dists[points[i], :]))
	end

	points
end

function maxmindp_random(g, k)
	randperm(nv(g))[1:k]
end

return struct test_struct
	k::Integer
end

test_struct(args) = test_struct(args...)

function run(settings, graph)
	return maxmindp_greedy_mindp(graph, settings.k)
  end

function getStruct()
  return test_struct
end

function getDefault()
	return Any[3]
end

function getName()
	"Max-min dp"
end

