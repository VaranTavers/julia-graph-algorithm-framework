import Pkg
Pkg.add("Graphs")
Pkg.add("Random")
Pkg.add("SimpleTraits")
Pkg.add("StatsBase")
using StatsBase
using SimpleTraits
using Graphs, Random

function calc_weight(w, clique)
    weight = 0
    for node in clique
        weight += w[node]
    end
    weight
end

function MWC(g,w)
    all = maximal_cliques(g)
    all[findmax(map(x->calc_weight(w, x), all))[2]]
end

function getName()
    "Maximum weigthed clique"
end

struct weightedCliqueSettings
end

function getStruct()
    weightedCliqueSettings
end

weightedCliqueSettings(args) = weightedCliqueSettings(args...)

function getDefault()
    return []
end

function run(setting :: weightedCliqueSettings, graph)
    w = [i for i in range(1, nv(graph))]
    MWC(graph, w)
end
