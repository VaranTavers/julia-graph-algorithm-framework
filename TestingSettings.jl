import JSON, CSV, DataFrames
import Cairo, Fontconfig
using DataFrames
using GraphIO
using GraphPlot
using Graphs
using Compose
using Colors
using Folds
using SimpleWeightedGraphs
include("src/fields_returner.jl")
include("src/create_struct.jl")

#import files from folder
begin
	dir = "src/ACO"
	file_paths = readdir(dir; join=true)
	file_paths = filter(name -> endswith(name, ".jl"),file_paths)
	my_modules = Vector()
	for file_path in file_paths
		println(file_path)
		push!(my_modules, ingredients(file_path))
	end
	my_modules
end

begin
	my_modules_names = [i.getName() for i in my_modules]
	modules_dict = Dict()
	for i in my_modules
		modules_dict[i.getName()] = i
	end
end

# TestSettings struct
struct TestSettings
    graphs::Vector{String}
    nrOfTests::Int64
    algorithm::String
    parameters::Vector{Float64}
end

struct Customization
    color::String
    targetColor::String
    nodelabel::Bool
    nodesize::Float64
end

function TestSettings(d::Dict)
    TestSettings(d["graphs"], d["nrOfTests"], d["algorithm"], d["parameters"])
end

function Customization(d::Dict)
    Customization(d["color"], d["targetColor"], d["nodelabel"], d["nodesize"])
end

# runs the algorithm for the given graphs
function run(testSettings::TestSettings)
    myStruct = createStruct(modules_dict[testSettings.algorithm].getStruct(), testSettings.parameters)

	dataFrame = DataFrame()
    algorithm_to_run = modules_dict[testSettings.algorithm].run

    graphs_fr = []
	for graph in testSettings.graphs
		# print(graph)
		graph_file_path = "graphs/" * graph
		push!(graphs_fr, loadgraph(graph_file_path, EdgeListFormat()))
	end

    for s in zip(testSettings.graphs, graphs_fr)
		graph_name, graph = s
        results = []
        for _ in 1:testSettings.nrOfTests
            push!(results, algorithm_to_run(myStruct, SimpleWeightedGraph(graph)))
        end

		println(results)
        dataFrame[!, graph_name] = results
    end

	return dataFrame
end

function saveGraphs(testSettings::TestSettings, customization::Customization, result::DataFrame)
    for graph in testSettings.graphs
        path = "graphs/" * graph
        draw = loadgraph(path, EdgeListFormat())

        # labels
        if customization.nodelabel == true
            nodelabel = collect(1:nv(draw))
        else
            nodelabel = nothing
        end

        for testIndex in 1:testSettings.nrOfTests
            # colors
            nodefillc = fill(parse(Colorant, customization.color), nv(draw))

            for resIndex in result[testIndex, graph]
                nodefillc[resIndex] = parse(Colorant, customization.targetColor)
            end

            # saving to file
            path = "images/" * testSettings.algorithm * "/" * string(testIndex)
            mkpath(path)

            graphPlot = gplot(draw, layout=spring_layout, NODESIZE=customization.nodesize, nodelabel=nodelabel, nodefillc=nodefillc)
            GraphPlot.draw(PNG(path * "/" * graph * ".png", 16cm, 16cm), graphPlot)
        end
    end
end

# reading TestSettings
testSettings = TestSettings(JSON.parsefile("settings1.json"))
customization = Customization(JSON.parsefile("customization.json"))
result = run(testSettings)
print(result)
CSV.write("result-" * testSettings.algorithm * ".csv", result)
saveGraphs(testSettings, customization, result)
