begin
    using Graphs
    using Distributed
    using CSV
    using DataFrames
    using SimpleWeightedGraphs
    using Folds
    using GraphPlot
end

function ingredients(path::String)
    # this is from the Julia source code (evalfile in base/loading.jl)
    # but with the modification that it returns the module instead of the last object
    name = Symbol(basename(path))
    m = Module(name)
    Core.eval(m,
        Expr(:toplevel,
            :(eval(x) = $(Expr(:core, :eval))($name, x)),
            :(include(x) = $(Expr(:top, :include))($name, x)),
            :(include(mapexpr::Function, x) = $(Expr(:top, :include))(mapexpr, $name, x)),
            :(include($path))))
    m
end

begin
    implementation_jl = ingredients("./implementation_aco.jl")
    import .implementation_jl: sample, ACOSettings, ACOInner, spread
end

struct ACOKSettings
    acos # :: ACOSettings
    k::Integer
    # There are situations when the ACO algorithm is unable to create the k subgraph
    # There is two options there:
    # 	false - skip the solution (faster, but might give worse answers, this is recommended if you have points with no neighbours)
    # 	true  - regenerate solution until a possible one is created (slower, but might give better answers)
    force_every_solution::Bool
    # If we force the length to be exactly k we may not get the correct answer since if we enter into a point from which only negative edges lead to anywhere the algorithm must choose a negative lenght even if there is a way to get to that point using an other route going back to where we come from. Now we can't let these answers become too big, since that would lead to infinite loops, so you can set an upper bound here:
    # If this equals k we will use the older method with the afforementioned error.
    solution_max_length::Integer
    ACOKSettings(α, β, n_a, ρ, ϵ, max_i, start_ph, k, f, s) = new(ACOSettings(α, β, n_a, ρ, ϵ, max_i, start_ph, (_, _) -> 1.0, (_, _) -> 1.0), k, f, s)
    ACOKSettings(α, β, n_a, ρ, ϵ, max_i, start_ph, e_f, c_s, k, f, s) = new(ACOSettings(α, β, n_a, ρ, ϵ, max_i, start_ph, e_f, c_s), k, f, s)
    ACOKSettings(acos, k, f, s) = new(acos, k, f, s)
end

# Calculates the probabilities of choosing edges to add to the solution.
function calculate_probabilities_old(inner::ACOInner, i, vars::ACOSettings, c)
    graph, n, η, τ = spread(inner)

    # graph.weights[i,j] *
    p = [findfirst(x -> x == j, c) == nothing ? (τ[i, j]^vars.α * η[i, j]^vars.β) : 0 for j in 1:n]
    if maximum(p) == 0
        p[i] = 1
    end
    s_p = sum(p)

    p ./= s_p

    p
end

# Calculates the probabilities of choosing edges to add to the solution.
function calculate_probabilities(inner::ACOInner, i, vars::ACOSettings)
    graph, n, η, τ = spread(inner)

    # graph.weights[i,j] *
    p = [(τ[i, j]^vars.α * η[i, j]^vars.β) for j in 1:n]

    if maximum(p) == 0
        p[i] = 1
    end
    s_p = sum(p)

    p ./= s_p

    p
end

function generate_s(inner::ACOInner, vars::ACOKSettings, i)
    if vars.k == vars.solution_max_length
        return generate_s_old(inner, vars)
    end

    points = zeros(Int64, vars.solution_max_length + 1)
    points[1] = i
    j = 2
    while length(unique(points)) - 1 < vars.k && j <= vars.solution_max_length

        points[j] = sample(calculate_probabilities(inner, points[j-1], vars.acos))
        j += 1
    end

    if j == vars.solution_max_length + 1 && length(unique(points)) - 1 < vars.k
        if vars.force_every_solution
            return generate_s(inner, vars, i)
        else
            return
        end
    end

    points[1:(j-1)]
end

# Constructs a new solution, the old way
function generate_s_old(inner::ACOInner, vars::ACOKSettings)
    i = rand(1:inner.n)
    points = zeros(Int64, vars.k)
    points[1] = i
    for i in 2:vars.k
        points[i] = sample(calculate_probabilities_old(inner, points[i-1], vars.acos, points))
        if points[i] == points[i-1]
            if vars.force_every_solution
                return generate_s(inner, vars)
            else
                return
            end
        end
    end

    points
end

function choose_iteration_best(inner::ACOInner, settings::ACOSettings, iterations)
    iterations = filter(x -> x != nothing, iterations)
    points = Folds.map(x -> settings.eval_f(inner.graph, settings.compute_solution(inner.graph, x)), iterations)
    index = argmax(points)
    (iterations[index], points[index])
end

begin
    fst((a, _)) = a
    snd((_, b)) = b
end

function calculate_η_ij(graph, i, j, m)
    if graph.weights[i, j] == 0
        return 0
    end

    graph.weights[i, j] - m + 1 + sum(graph.weights[:, j])
end

function calculate_η(graph)
    n = nv(graph)

    m = minimum(graph.weights)
    if m >= 0
        m = 0
    end
    η = [calculate_η_ij(graph, i, j, m) for i in 1:n, j in 1:n]

    η
end

function get_weight(g, (x, y))
    g.weights[x, y]
end

function calculate_heaviness(graph, c)
    sum(map(x -> get_weight(graph, x), c))
end

function compute_solution(graph, s)
    edges_orig = [(0, 0) for i in 1:(length(s)-1)]
    for i in 1:(length(s)-1)
        if s[i] < s[i+1]
            edges_orig[i] = (s[i], s[i+1])
        else
            edges_orig[i] = (s[i+1], s[i])
        end
    end
    edges_else = []
    for i in 1:(length(s)-2)
        for j in (i+2):length(s)
            if graph.weights[s[i], s[j]] > 0
                if s[i] < s[j]
                    push!(edges_else, (s[i], s[j]))
                else
                    push!(edges_else, (s[j], s[i]))
                end
            end
        end
    end
    append!(edges_orig, edges_else)

    unique(edges_orig)
end

function ACOK(graph, vars::ACOKSettings, η, τ)
    #Set parameters and initialize pheromone traits.
    n, _ = size(η)
    inner = ACOInner(graph, n, η, τ)

    @assert nv(graph) >= vars.k
    @assert vars.k <= vars.solution_max_length
    sgb = [i for i in 1:n]
    sgb_val = -1000
    τ_max = vars.acos.starting_pheromone_ammount
    τ_min = 0

    # While termination condition not met
    for i in 1:vars.acos.max_number_of_iterations
        # Construct new solution s according to Eq. 2

        if i < vars.acos.max_number_of_iterations - 3
            S = Folds.map(x -> generate_s(inner, vars, rand(1:inner.n)), zeros(vars.acos.number_of_ants))
        else
            S = Folds.map(x -> generate_s(inner, vars, x), 1:inner.n)
        end

        if length(filter(x -> x != nothing, S)) > 0
            # Update iteration best
            (sib, sib_val) = choose_iteration_best(inner, vars.acos, S)
            if sib_val > sgb_val
                sgb_val = sib_val
                sgb = sib

                # Compute pheromone trail limits
                τ_max = sgb_val / (1 - vars.acos.ρ)
                τ_min = vars.acos.ϵ * τ_max
            end
            # Update pheromone trails
            # TODO: test with matrix sum
            τ .*= (1 - vars.acos.ρ)
            for (a, b) in zip(sib, sib[2:end])
                τ[a, b] += sib_val
                τ[b, a] += sib_val

            end
        end
        τ = min.(τ, τ_max)
        τ = max.(τ, τ_min)

    end

    vars.acos.compute_solution(inner.graph, sgb), τ
end

function ACOK(graph, vars::ACOKSettings, η)
    n, _ = size(η)
    τ = ones(n, n) .* vars.acos.starting_pheromone_ammount
    r, _ = ACOK(graph, vars, η, τ)

    r
end


function ACOK_get_pheromone(graph, vars::ACOKSettings, η)
    n, _ = size(η)
    τ = ones(n, n) .* vars.acos.starting_pheromone_ammount
    ACOK(graph, vars, η, τ)
end

function copy_replace_funcs(vars_base::ACOKSettings, eval_f, c_s)
    ACOKSettings(
        ACOSettings(
            vars_base.acos.α,
            vars_base.acos.β,
            vars_base.acos.number_of_ants,
            vars_base.acos.ρ,
            vars_base.acos.ϵ,
            vars_base.acos.max_number_of_iterations,
            vars_base.acos.starting_pheromone_ammount,
            eval_f,
            c_s
        ),
        vars_base.k,
        vars_base.force_every_solution,
        vars_base.solution_max_length,
    )
end

function HeaviestACOK(graph, vars_base::ACOKSettings, τ)
    η = calculate_η(graph)

    vars = copy_replace_funcs(vars_base, calculate_heaviness, compute_solution)

    ACOK(graph, vars, η, τ)
end

function HeaviestACOK(graph, vars_base::ACOKSettings)
    η = calculate_η(graph)

    vars = copy_replace_funcs(vars_base, calculate_heaviness, compute_solution)

    ACOK(graph, vars, η)
end

function HeaviestACOK_get_pheromone(graph, vars_base::ACOKSettings)
    η = calculate_η(graph)

    vars = copy_replace_funcs(vars_base, calculate_heaviness, compute_solution)

    ACOK_get_pheromone(graph, vars, η)
end

function solution_to_community(graph, solution)
    n = nv(graph)

    f = fst.(solution)
    s = snd.(solution)
    append!(f, s)

    [findfirst(x -> x == i, f) != nothing ? 1 : 0 for i in 1:n]
end

#########################################################
struct TestSettingsTBD # structure TBD
    graph::String
    params::Array
end

struct UniqStructWithAUnicAndCreativeName # naming TBD of course
end

#function HeaviestACOK(graph, vars_base::ACOKSettings)
function graphLoader(name::String)
    g = loadgraph("./graphs/$name", SWGFormat())
    gplot(g, nodelabel=1:nv(g))
    return g
end


function bulidACOSettingsStruct(settings)
    vars = ACOSettings(
        settings.params[1], # α
        settings.params[2], # β
        settings.params[3], # number_of_ants
        settings.params[4], # ρ
        settings.params[5], # ϵ
        settings.params[6], # max_number_of_iterations
        settings.params[7] # starting_pheromone_ammount
    )

    k = settings.params[8]
    vars3 = ACOKSettings(
        vars,
        k, # subgraph
        false, #force every solution
        Integer(ceil(k * 3)) #solution max size
    )

    return vars3
end

function run(settings::TestSettingsTBD, uniq::UniqStructWithAUnicAndCreativeName)
    g = graphLoader(settings.graph)
    params = bulidACOSettingsStruct(settings)
    return HeaviestACOK(g, params)
end


# ╔═╡ e8f705bb-be85-48aa-a25e-0f9e2921f6a3
begin
    # g = loadgraph("../../graphs/heavy/changhonghao.lgz", SWGFormat())

    # vars = ACOSettings(
    #		1, # α
    #		2, # β
    #		30, # number_of_ants
    #		0.8, # ρ
    #		0.005, # ϵ
    #		100, # max_number_of_iterations
    #		3 # starting_pheromone_ammount
    #	)
    #c = HeaviestACOK(g, ACOKSettings(vars, 12, false, 20))
    #calculate_heaviness(g, c)

end