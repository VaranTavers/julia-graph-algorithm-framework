function ingredients(path::String)
	# this is from the Julia source code (evalfile in base/loading.jl)
	# but with the modification that it returns the module instead of the last object
	name = Symbol(basename(path))
	m = Module(name)
	Core.eval(m,
        Expr(:toplevel,
             :(eval(x) = $(Expr(:core, :eval))($name, x)),
             :(include(x) = $(Expr(:top, :include))($name, x)),
             :(include(mapexpr::Function, x) = $(Expr(:top, :include))(mapexpr, $name, x)),
             :(include($path))))
	m
end

begin
  implementation_k_heavy_jl = ingredients("./ACO/implementation_k_heavy.jl")
  import .implementation_k_heavy_jl: run, TestSettingsTBD, UniqStructWithAUnicAndCreativeName #we wont be able to use a single TestSettings?
end

myArray = [1, 2, 5, 0.8, 0.005, 10, 300, 5] # values stolen from the github repo
settings = TestSettingsTBD(
  "changhonghao.lgz",
  myArray
)

print(run(settings, UniqStructWithAUnicAndCreativeName()))