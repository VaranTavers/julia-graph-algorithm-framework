function ingredients(path::String)
	# this is from the Julia source code (evalfile in base/loading.jl)
	# but with the modification that it returns the module instead of the last object
	name = Symbol(basename(path))
	m = Module(name)
	Core.eval(m,
        Expr(:toplevel,
             :(eval(x) = $(Expr(:core, :eval))($name, x)),
             :(include(x) = $(Expr(:top, :include))($name, x)),
             :(include(mapexpr::Function, x) = $(Expr(:top, :include))(mapexpr, $name, x)),
             :(include($path))))
	m
end

begin
	implementation_jl = ingredients("./ACO/implementation_k_dense.jl")
	import .implementation_jl: sample, ACOKSettings, ACOInner, spread
end


function returnFields(myStruct)
    myStructsFieldTypes = convert(Vector{Any}, [x for x in fieldtypes(myStruct)])
    myStructsFieldNames = convert(Vector{Any}, [x for x in fieldnames(myStruct)])
    for i in range(1, length(myStructsFieldNames))
        if isstructtype(myStructsFieldTypes[i])
            x = returnFields(myStructsFieldTypes[i])
            deleteat!(myStructsFieldTypes, i)
            deleteat!(myStructsFieldNames, i)
            append!(myStructsFieldTypes, x[2])
            append!(myStructsFieldNames, x[1])
        end
    end

    return myStructsFieldNames, myStructsFieldTypes
end


# To create the object from ACOKSettings it is known that we got the result with calling the function with ACOKSettings,
#so we can say ACOKSetting(param1, param2, param3, structType1([param1OfStructType, param2OfStructType]), ...)

#TODO: add field types where needed at other algorithms(implementation_heavy.jl, and others)