#https://juliagraphs.org/Graphs.jl/dev/algorithms/community/
import Pkg
Pkg.add("Graphs")
Pkg.add("Random")
Pkg.add("SimpleTraits")
Pkg.add("StatsBase")
using StatsBase
using SimpleTraits
using Graphs, Random


function beklikk(aktualis_klikk, g)
    ind = length(aktualis_klikk)
    for i in range(1, ind - 1)
        for j in range(i + 1, ind)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                return false
            end
        end
    end
    return true
end
function hianyzo_el_db(aktualis_klikk, g)
    n = length(aktualis_klikk)
    db = 0
    for i in range(1, n - 1)
        for j in range(i + 1, n)
            if !(has_edge(g, aktualis_klikk[i], aktualis_klikk[j]))
                db += 1
            end
        end
    end
    return db
end
function minden_klikk(populacio, g)
    mapreduce(t -> beklikk(t[1], g), &, populacio)
end


# szetvalogatja a DE egyedet klikk es nem_klikk halmazokra
function szetvalogat(egyed, g)
    v = vertices(g)
    klikk = []
    nem_klikk = []
    for i in 1:nv(g)
        if egyed[i] == 1
            push!(klikk, v[i])
        else
            push!(nem_klikk, v[i])
        end
    end

    (klikk, nem_klikk)
end
#visszateriti a populcaio GP szerinti reprezentaciojat
function toArrays(populacio, g)
    map(x -> szetvalogat(x, g), populacio)
end
#mu darad valoszinusegeket general, ugy, hogz majd exponencialis eloszlas szerint vehessek random egyedeket
function generate_prob(mu)
    rng = MersenneTwister(1234)
    k = randexp(rng, Float32, mu)
    s = sum(k)
    sort(map(x -> x / s, k), rev=true)
end
function meretez_DE(uj_egyed, n_klikk)
    uj = copy(uj_egyed)
    n = length(uj)
    while count(x -> x == 1, uj) > n_klikk
        i = rand(1:n)
        while uj[i] == 0
            i = rand(1:n)
        end
        uj[i] = 0
    end

    while count(x -> x == 1, uj) < n_klikk
        i = rand(1:n)
        while uj[i] == 1
            i = rand(1:n)
        end
        uj[i] = 1
    end

    uj
end
function DE_mutacio_megtart_elemszam(populacio, mu, n_klikk, pc)
    p = generate_prob(mu)

    [
        begin
            r1 = sample(populacio, Weights(p), 1)[1] #elso egyedet exponencialis valoszinuseg szerint veszi, igy nagyobb valosyinuseggel valszt jo egyedet
            r2 = rand(populacio)
            r3 = rand(populacio)
            uj = map((e1, e2, e3) ->
                    if e2 == e3 || rand() > pc
                        e1
                    else
                        e2
                    end, r1, r2, r3)

            meretez_DE(uj, n_klikk)
        end
        for i in 1:mu
    ]
end
function jobb(klikk1, klikk2, g)
    h1 = hianyzo_el_db(klikk1, g)
    h2 = hianyzo_el_db(klikk2, g)
    if h1 == h2
        return length(klikk1) > length(klikk2)
    else
        return h1 < h2
    end
end
function DE_szelekcio(populacio, u, g)
    map((egyed1, egyed2) ->
            if jobb(szetvalogat(egyed1, g)[1], szetvalogat(egyed2, g)[1], g)
                egyed1
            else
                egyed2
            end,
        populacio, u)
end
function DE(kezdeti_populcaio, g, n_klikk, pc, lep)
    populacio = copy(kezdeti_populcaio)
    mu = length(populacio)
    while lep > 0 && !minden_klikk(toArrays(populacio, g), g)
        sort(populacio, lt=(x, y) -> jobb(szetvalogat(x, g)[1], szetvalogat(y, g)[1], g))
        v = DE_mutacio_megtart_elemszam(populacio, mu, n_klikk, pc)
        populacio = DE_szelekcio(populacio, v, g)
        lep -= 1
    end

    populacio
end
function DE_init_populacio_2(nv, mu)
    [
        begin
            k = [0 for i in 1:nv]
            csp1 = rand(1:nv)
            csp2 = rand(1:nv)
            while csp1 == csp2
                csp2 = rand(1:nv)
            end
            k[csp1] = 1
            k[csp2] = 1
            k
        end
        for i in 1:mu]
end
function DE_tagit_populacio(t)
    n = length(t)
    egyed = copy(t)
    csp = rand(1:n)
    while egyed[csp] == 1
        csp = rand(1:n)
    end
    egyed[csp] = 1
    egyed
end

function max_klikk_DE(g, mu=20, pc=0.9, lep=10000)
    n_klikk = 2
    n = nv(g)
    populacio = DE_init_populacio_2(n, mu)

    populacio = DE(populacio, g, n_klikk, pc, lep)
    max_klikk = []

    while n_klikk < n && beklikk(szetvalogat(populacio[1], g)[1], g)
        max_klikk = copy(szetvalogat(populacio[1], g)[1])

        n_klikk += 1
        populacio = map(DE_tagit_populacio, populacio)
        populacio = DE(populacio, g, n_klikk, pc, lep)
    end

    if n_klikk == n && beklikk(vertices(g), g)
        max_klikk = vertices(g)
    elseif beklikk(szetvalogat(populacio[1], g)[1], g)
        max_klikk = copy(szetvalogat(populacio[1], g)[1])
    end
    max_klikk
end

function beolvas(file_nev)
    g = SimpleGraph()
    open(file_nev, "r") do file
        s = readline(file)
        sp = split(s, " ")
        n = parse(Int64, sp[1])
        m = parse(Int64, sp[2])
        g = SimpleGraph(n)

        for i in 1:m
            s = readline(file)
            sp = split(s, " ")
            e1 = parse(Int64, sp[1])
            e2 = parse(Int64, sp[2])
            add_edge!(g, e1, e2)
        end
    end
    g
end

struct MC_DE_Settings
    mu::Integer
    pc::Real
    lep::Integer
end

MC_DE_Settings(args) = MC_DE_Settings(args...)

function run(setting::MC_DE_Settings, graph)
    #TODO read graph in
    max_klikk_DE(graph, setting.mu, setting.pc, setting.lep)
end

function getStruct()
    return MC_DE_Settings
end

function getDefault()
    return Any[25, 0.8, 2000]
end

function getName()
    "Max clique - Differential evolution"
end