using Random

# until these functions get implemented, thses are their placeholders
function maxClick(graph, parameters)
    a = Vector{Int}()

    while size(a)[1] != 4
        b = rand(1:36)

        if !(b in a)
            append!(a, b)
        end
    end

    return a
end

function ACO(graph, parameters)
    a = Vector{Int}()

    while size(a)[1] != 4
        b = rand(1:36)

        if !(b in a)
            append!(a, b)
        end
    end

    return a
end
